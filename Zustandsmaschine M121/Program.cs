﻿using System;

namespace Zustandsmaschine_M121
{
    class Program
    {
        public enum status
        {
            sign,
            afterSign,
            beforeDecimalDigit,
            decimalDigit
        }
        static void Main(string[] args)
        {
            //declare variables and set values
            double result = 0;
            bool signNegative = false;
            double number = 0;
            string line;
            double decimalDigitFactor = 1;

            //open file
            System.IO.StreamReader file = new System.IO.StreamReader(@"Rand10000001.txt");
            var status = Program.status.sign;

            while ((line = file.ReadLine()) != null)
            {
                foreach (char item in line)
                {
                    switch (status)
                    {
                        case status.sign:
                            sign(item);
                            break;
                        case status.afterSign:
                            afterSign(item);
                            break;
                        case status.beforeDecimalDigit:
                            beforeDecimalDigit(item);
                            break;
                        case status.decimalDigit:
                            decimalDigit(item);
                            break;
                        default:
                            throw new ArgumentException("Argument for status isn't known");
                    }
                }
            }
            Console.WriteLine("The result is: " + result);

            //Here a multiple functions
            //each function is a status of the finite state machine
            void sign(char _char)
            {
                if (_char == '+' || _char == '-')
                {
                    if (_char == '-')
                    {
                        signNegative = true;
                    }
                    status = status.afterSign;
                }
                else if(char.IsDigit(_char))
                {
                    number *= 10;
                    number += char.GetNumericValue(_char);
                    status = status.beforeDecimalDigit;
                }
            }

            void afterSign(char _char)
            {
                if (char.IsDigit(_char))
                {
                    number *= 10;
                    number += char.GetNumericValue(_char);
                    status = status.beforeDecimalDigit;
                }
            }
            void beforeDecimalDigit(char _char)
            {
                if (char.IsDigit(_char))
                {
                    number *= 10;
                    number += char.GetNumericValue(_char);
                }
                else if (_char == '.')
                {
                    status = status.decimalDigit;
                }
            }

            void decimalDigit(char _char)
            {
                if (_char != ',')
                {
                    decimalDigitFactor /= 10;
                    number += char.GetNumericValue(_char) * decimalDigitFactor;
                }
                else
                {
                    if (signNegative)
                    {
                        result -= number;
                    }
                    else
                    {
                        result += number;
                    }
                    status = status.sign;
                    number = 0;
                    signNegative = false;
                    decimalDigitFactor = 1;
                    status = status.sign;
                }
            }
        }
    }
}
